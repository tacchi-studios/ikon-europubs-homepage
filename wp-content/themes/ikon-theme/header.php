<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
</head>

<!-- Custom Scripts -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/headroom.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/custom.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/cssua.min.js"></script>

<!-- Custom javasctipt for specific page (company) -->
<?php if (is_page('Company')): ?><script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/custom-company.js"></script><?php endif; ?>
<?php if (is_page('会社情報')): ?><script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/custom-company.js"></script><?php endif; ?>

<!-- Fonts -->
<style>
@import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900');
@import url('https://fonts.googleapis.com/css?family=Lato:300,400,700,900');

@import url("//hello.myfonts.net/count/341a8a");

@font-face {font-family: 'FrutigerLTStd-UltraBlack';
src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/341A8A_0_0.eot');
src: url('<?php bloginfo('stylesheet_directory'); ?>/fonts/341A8A_0_0.eot?#iefix') format('embedded-opentype'),
url('<?php bloginfo('stylesheet_directory'); ?>/fonts/341A8A_0_0.woff2') format('woff2'),
url('<?php bloginfo('stylesheet_directory'); ?>/fonts/341A8A_0_0.woff') format('woff'),
url('<?php bloginfo('stylesheet_directory'); ?>/fonts/341A8A_0_0.ttf') format('truetype');}

</style>

<body <?php body_class(); ?>>

<div class="hfeed site" id="page">

	<!-- ******************* The Navbar Area ******************* -->

		<nav class="navbar navbar-toggleable white no-pad transition desktop-view">


	    	<div class="container ikon-container nav no-pad transition">
					<!-- Logo -->
			    <a class="logo" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
				    <?php if(ICL_LANGUAGE_CODE=='en'): ?>
				        <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/logo.svg" title="Ikon Europubs logo" alt="ikon-europubs-logo" width="260" height="70">
				    <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
				    	  <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/logo.svg" title="アイコン・ユーローパブ・ロゴ" alt="アイコンユーローパブロゴ" width="260" height="70">
				    <?php endif; ?>
			    </a>

			    <!-- Wordpress nav -->
				<div id="navbarNavDropdown" class="collapse navbar-collapse justify-content-end">
					<?php wp_nav_menu(
						array(
							'theme_location'  => 'primary',
							'menu_class'      => 'navbar-nav',
							'fallback_cb'     => '',
							'menu_id'         => 'main-menu',
							'walker'          => new WP_Bootstrap_Navwalker(),
						)
					); ?>
					<!-- CTA & language nav desktop -->
				    <ul id="cta-nav" class="navbar-nav">
			            <li>
				            <a target="_blank" href="<?php echo get_site_url(); ?>/wp-content/uploads/ikon-europubs-catalog.pdf">
					            <button class="btn ikon primary">
					                <h6 class="white-text">
						                <?php if(ICL_LANGUAGE_CODE=='en'): ?>download catalog<?php elseif(ICL_LANGUAGE_CODE=='ja'): ?><span>カタログを</span><span>ダウンロードする</span><?php endif; ?>
						            </h6>
					            </button>
				            </a>
			            </li>
			            <li><?php language_selector(); ?></li>
				    </ul>
			    </div>

		  	</div><!-- .container -->

		</nav><!-- .site-navigation -->

		<nav class="navbar navbar-toggleable border white no-pad transition mobile-view">
			<!-- CTA & language nav desktop -->
			<?php wp_nav_menu(
				array(
					'theme_location'  => 'primary',
					'menu_class'      => 'navbar-nav',
					'fallback_cb'     => '',
					'menu_id'         => 'main-menu',
					'walker'          => new WP_Bootstrap_Navwalker(),
				)
			); ?>
		</nav>


		<div class="vs-100"></div>

<script>var myElement = document.querySelector(".mobile-view");var headroom  = new Headroom(myElement);headroom.init();</script>
