<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

?>

<footer class="site-footer border">
    <div class="border"></div>
    <div class="vs-40"></div>
    <div class="ikon-container nav no-pad site-info">
        <!-- Logo -->
        <a class="logo" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/logo.svg"></a>
        <!-- Contact details -->
        <div class="contact-info">
      	  <?php if(ICL_LANGUAGE_CODE=='en'): ?>
        		<p class="sm">Daikyocho 14-5, Shinjuku-ku, Tokyo 160-0015<span>&nbsp;&nbsp;|&nbsp;&nbsp;</span><a target="_blank" class="map-link brown-text" href="https://www.google.co.jp/maps/place/35%C2%B041'05.8%22N+139%C2%B043'01.6%22E/@35.6849303,139.7171331,21z/data=!4m5!3m4!1s0x0:0x0!8m2!3d35.684944!4d139.717101=en"><b>View Map</b></a><br>
        		Email:  web_contact@ikon-europubs.com<br>
        		Tel: 03-5369-3601</p>
      		<?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
      			<p class="xs"><b>Ikon Europubs　株式会社</b><br>
      			〒160-0015 東京都新宿区大京町14-5<span>&nbsp;&nbsp;|&nbsp;&nbsp;</span><a target="_blank" class="map-link brown-text" href="https://www.google.co.jp/maps/place/35%C2%B041'05.8%22N+139%C2%B043'01.6%22E/@35.6849303,139.7171331,21z/data=!4m5!3m4!1s0x0:0x0!8m2!3d35.684944!4d139.717101=ja"><b>地図を見る</b></a><br>
      			Tel: 03-5369-3601&nbsp;&nbsp;|&nbsp;&nbsp;Fax: 03-5369-3602<br>Email: web_contact@ikon-europubs.com</p>
      		<?php endif; ?>
        </div>
        <!-- Legal -->
        <?php if(ICL_LANGUAGE_CODE=='en'): ?><img class="legal" src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-underage-drinking-label-english.svg" title="Stop! Underage drinking" alt="stop-underage-drinking-icon"><?php elseif(ICL_LANGUAGE_CODE=='ja'): ?><img class="legal" src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-underage-drinking-label-japanese.svg" title="STOP！ 未成年者飲酒" alt="stop未成年者飲酒アイコン"><?php endif; ?>
    </div>
  <div class="vs-40"></div>
  <div class="border"></div>
  <p class="xs copyright center-aligned">© copyright Ikon Europubs KK. All rights reserved.&nbsp;&nbsp;|&nbsp;&nbsp;<a class="brown-text" href="<?php echo get_permalink( get_page_by_path( 'privacy policy' ) ) ?>"><b>Privacy policy</b></a></p>
</footer>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-3286037-4', 'auto');
  ga('send', 'pageview');
</script>

</body>

</html>