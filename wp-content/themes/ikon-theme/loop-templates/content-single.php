<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

?>

<div class="vs-60 mobile"></div>
<div class="vs-40 mobile"></div>
<div class="vs-10 tablet"></div>
<div class="vs-10"></div>

<div class="wrapper col-md-12 no-pad">
    <div id="<?php echo get_post_meta( get_the_ID(), 'anchor', true ); ?>" class="anchor"></div>
    <div class="col-md-12 vs-60"></div>
  <div class="col-lg-5 col-md-12 col-sm-12 no-pad">
    <img src="<?php the_post_thumbnail_url( 'full' ); ?>" title="<?php echo get_post_meta( get_the_ID(), 'image_alt_text', true ); ?>" alt="<?php echo get_post_meta( get_the_ID(), 'image_alt_text', true ); ?>">
    <div class="vs-10"></div>
  </div>
  <div class="col-lg-7 col-md-12 col-sm-12">
    <h6 class="brown-text"><b>
      <?php if( get_field('label') == 'Style' ): ?>
        <?php if(ICL_LANGUAGE_CODE=='en'): ?>
          Style
        <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
          スタイル
        <?php endif; ?>
      <?php elseif ( get_field('label') == 'Category' ): ?>
        <?php if(ICL_LANGUAGE_CODE=='en'): ?>
          Category
        <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
          カテゴリー
        <?php endif; ?>
      <?php endif; ?>
    </b></h6>
    <h2 class="uppercase"><b><?php the_title(); ?></b></h2>
      <p class="grey-text"><?php the_content(); ?></p>
      <div class="food-section">
        <table>
          <tr>
            <td>
                  <?php if(ICL_LANGUAGE_CODE=='en'): ?>
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/food-icon.svg" title="Ikon Europubs Knife and Fork" alt="ikon-europubs-knife-and-fork">
              <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/food-icon.svg" title="アイコン・ユーローパブナイフとフォーク" alt="アイコン・ユーローパブナイフとフォーク">
              <?php endif; ?>
            </td>
            <td>
              <p class="sm brown-text"><?php echo get_post_meta( get_the_ID(), 'food', true ); ?></p>
            </td>
          </tr>
        </table>
      </div>
  </div>
</div>
