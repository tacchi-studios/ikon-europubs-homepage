<?php
/**
 * @package understrap
 */
?>

<article>
	<h2><?php the_title(); ?></h2>
	<div class="vs-30"></div>
	<?php the_content(); ?>
</article>

