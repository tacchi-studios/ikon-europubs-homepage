<?php
/**
 * @package understrap
 */
?>
<?php if( get_field('label') == 'Style' ): ?>
  <div class="col-4">
    <a href="<?php the_permalink() ?>">
    <?php $image = get_field('thumbnail_image'); ?><img src="<?php echo $image['url']; ?>" title="<?php echo get_post_meta( get_the_ID(), 'image_alt_text', true ); ?>" alt="<?php echo get_post_meta( get_the_ID(), 'image_alt_text', true ); ?>"/>
    <div class="vs-20"></div>
    <p class="sm"><?php echo get_post_meta( get_the_ID(), 'thumbnail_description', true ); ?></p>
    </a>
  </div>
<?php endif; ?>



