<?php
/**
 * @package understrap
 */
?>
<article class="jobs">
    <div class="vs-20"></div>
	<h4><b><?php the_title(); ?></b></4>
	<p class="grey-text sm"><?php the_content(); ?></p>
    <?php $table = get_field( '求人データ表' );if ( $table ) {echo '<table border="0">';if ( $table['header'] ) {echo '<thead>';echo '<tr>';foreach ( $table['header'] as $th ) {echo '<th>';echo $th['c'];echo '</th>';}echo '</tr>';echo '</thead>';}echo '<tbody>';foreach ( $table['body'] as $tr ) {echo '<tr>';foreach ( $tr as $td ) {echo '<td>';echo $td['c'];echo '</td>';}echo '</tr>';}echo '</tbody>';echo '</table>';}
    ?>
	<div class="vs-40 border"></div>
	<div class="vs-20"></div>
</article>
