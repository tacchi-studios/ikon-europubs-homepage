jQuery(document).ready(function($) {

    $(document).on('click', '.company-menu a', function(event){
	    event.preventDefault();

	    $('html, body').animate({
	        scrollTop: $( $.attr(this, 'href') ).offset().top
	    }, 750);
	});

	var targetOffset = $("#about-ikon-europubs").offset().top;

	var $w = $(window).scroll(function(){
	    if ( $w.scrollTop() > targetOffset ) {
	        $('.company-menu').addClass('fixed');
	    } else {
	    	$('.company-menu').removeClass('fixed');
	    }
	});

	var targetOffset2 = $("#company-outline").offset().top-100;

	var $w = $(window).scroll(function(){
	    if ( $w.scrollTop() > targetOffset2 ) {
	        $('a.about').removeClass('toggled');
	        $('a.company').addClass('toggled');
	    } else {
	    	$('a.company').removeClass('toggled');
	    	$('a.about').addClass('toggled');
	    }
	});

});