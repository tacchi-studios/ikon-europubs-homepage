 jQuery(document).ready(function(){
     //execute code when document is ready
     jQuery('.note1').hide();
     jQuery(document).on("change", "#your-interest", function () {
         jQuery('.note1').hide();
         var neededId = jQuery(this).val();
         var divToShow = jQuery(".note1").filter("[id='" + neededId + "']");
         divToShow.show();
     });

     jQuery('.note2').hide();
     jQuery(document).on("change", "#your-business", function () {
         jQuery('.note2').hide();
         var neededId = jQuery(this).val();
         var divToShow = jQuery(".note2").filter("[id='" + neededId + "']");
         divToShow.show();
     });

});

jQuery(document).ready(function($) {

	$(document).on('click', '.home .thumbnails a', function(event){
	    event.preventDefault();

	    $('html, body').animate({
	        scrollTop: $( $.attr(this, 'href') ).offset().top
	    }, 750);
	});

});



