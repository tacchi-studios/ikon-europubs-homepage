<?php
/**
 * Template Name: Ikon Contact page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header(); ?>

<div class="vs-80 mobile"></div>
<div class="vs-60 mobile"></div>
<div class="vs-40 tablet"></div>
<div class="vs-60"></div>

<div class="ikon-container">

    <h2><?php if(ICL_LANGUAGE_CODE=='en'): ?>Contact Form<?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>お問い合わせフォーム<?php endif; ?></h2>
    <div class="vs-20"></div>

	<div class="entry-content">
	    <?php the_content();?>
	</div>

</div>

<div class="vs-80"></div>

<!-- FOOTER -->
<?php get_footer(); ?>

