<?php
/**
 * Template Name: example
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header(); ?>

<div class="vs-80 mobile"></div>
<div class="vs-60 mobile"></div>
<div class="vs-40 tablet"></div>
<div class="vs-60"></div>

<div class="ikon-container lg">
	<?php while ( have_posts() ) : the_post(); ?>
	<?php get_template_part( 'loop-templates/content', 'single' ); ?>
        <h1 class="h1-size">hello!</h1>
        <h2 class="h2-size">hello!</h2>
        <h3 class="h3-size">hello!</h3>
        <h4 class="h4-size">hello!</h4>
        <h5 class="h5-size">hello!</h5>
        <p class="p-lg-size">hello!</p>
        <p class="p-md-size">hello!</p>
        <p class="p-sm-size">hello!</p>
    <?php endwhile; // end of the loop. ?>
</div>

<div class="vs-80"></div>

<!-- FOOTER -->
<?php get_footer(); ?>
