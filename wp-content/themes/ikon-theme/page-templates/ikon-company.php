<?php
/**
 * Template Name: Ikon Company page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header(); ?>

<!-- COMPANY BANNER-->
<div class="vs-80 mobile"></div>
<div class="ikon-banner vh280">
    <div class="banner-img" title="Beer glasses at an restaurant" alt="beer-glasses-restaurant"></div>
</div>

<div class="vs-60"></div>

<!-- CONTAINER -->
<div class="ikon-container company">
    <!-- Menu -->
	<div class="menu-container no-pad">
    <div class="company-menu">
      <a class="about toggled transition" href="#about-ikon-europubs"><div class="square-arrow"></div><p class=""><i class="fa fa-angle-right dark-yellow-text" aria-hidden="true"></i>&nbsp;&nbsp;<?php if(ICL_LANGUAGE_CODE=='en'): ?><b>About Ikon Europubs</b><?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>会社情報<?php endif; ?></b></p></a>
      <a class="company transition" href="#company-outline"><div class="square-arrow"></div><p class=""><i class="fa fa-angle-right dark-yellow-text" aria-hidden="true"></i>&nbsp;&nbsp;<?php if(ICL_LANGUAGE_CODE=='en'): ?><b>Company Outline</b><?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>会社概要<?php endif; ?></p></a>
    </div>

	</div>
	<!-- Content -->
	<div class="article-container no-pad">

	    <!-- About Ikon Europubs -->
		<article>
		  <div id="about-ikon-europubs" class="anchor"></div>

	        <!-- Section 1 -->
		    <h3><?php echo get_post_meta( get_the_ID(), 'company_title', true ); ?></h3>
		    <div class="vs-20"></div>
		    <p><?php echo get_post_meta( get_the_ID(), 'company_description', true ); ?></p>
		    <div class="vs-20"></div>

            <!-- Section 2 -->
            <h4 class="black-text"><?php echo get_post_meta( get_the_ID(), 'support_title', true ); ?></h4>
		    <p><?php echo get_post_meta( get_the_ID(), 'support_description', true ); ?></p>
		    <div class="vs-20"></div>

		    <!-- Section 3 -->
		    <h4 class="black-text"><?php echo get_post_meta( get_the_ID(), 'distribution_chains_title', true ); ?></h4>
		    <?php if(ICL_LANGUAGE_CODE=='en'): ?><p><?php echo get_post_meta( get_the_ID(), 'distribution_chains_description', true ); ?></p><?php endif; ?>
		    <div class="vs-10"></div>

	        <!-- Section 3 Distribution company logos -->
		    <div class="logo-container center-aligned flex-wrap">
			    <div class="vs-30"></div>

			    <div class="col-sm-4 col-6 flex-column"><?php $image = get_field('distribution_logo_1'); ?>
			        <?php if(ICL_LANGUAGE_CODE=='en'): ?>
				        <img src="<?php echo $image['url']; ?>" alt="<?php echo get_post_meta( get_the_ID(), 'distribution_title_1', true ); ?>" title="<?php echo get_post_meta( get_the_ID(), 'distribution_title_1', true ); ?>">
				    <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
				        <img src="<?php echo $image['url']; ?>" alt="国分グループ本社株式会社" title="国分グループ本社株式会社">
				    <?php endif; ?>
				    <p class="sm"><?php echo get_post_meta( get_the_ID(), 'distribution_title_1', true ); ?></p>
			    </div>

			    <div class="col-sm-4 col-6 flex-column"><?php $image = get_field('distribution_logo_2'); ?>
			        <?php if(ICL_LANGUAGE_CODE=='en'): ?>
				        <img src="<?php echo $image['url']; ?>" alt="<?php echo get_post_meta( get_the_ID(), 'distribution_title_2', true ); ?>" title="<?php echo get_post_meta( get_the_ID(), 'distribution_title_2', true ); ?>">
				    <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
				        <img src="<?php echo $image['url']; ?>" alt="三井食品株式会社" title="三井食品株式会社">
				    <?php endif; ?>
				    <p class="sm"><?php echo get_post_meta( get_the_ID(), 'distribution_title_2', true ); ?></p>
				</div>

			    <div class="col-sm-4 col-6 flex-column"><?php $image = get_field('distribution_logo_3'); ?>
			        <?php if(ICL_LANGUAGE_CODE=='en'): ?>
				        <img src="<?php echo $image['url']; ?>" alt="<?php echo get_post_meta( get_the_ID(), 'distribution_title_3', true ); ?>" title="<?php echo get_post_meta( get_the_ID(), 'distribution_title_3', true ); ?>">
				    <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
				        <img src="<?php echo $image['url']; ?>" alt="伊藤忠食品株式会社" title="伊藤忠食品株式会社">
				    <?php endif; ?>
				    <p class="sm"><?php echo get_post_meta( get_the_ID(), 'distribution_title_3', true ); ?></p>
			    </div>

			    <div class="col-sm-4 col-6 flex-column"><?php $image = get_field('distribution_logo_4'); ?>
			        <?php if(ICL_LANGUAGE_CODE=='en'): ?>
				        <img src="<?php echo $image['url']; ?>" alt="<?php echo get_post_meta( get_the_ID(), 'distribution_title_4', true ); ?>" title="<?php echo get_post_meta( get_the_ID(), 'distribution_title_4', true ); ?>">
				    <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
				        <img src="<?php echo $image['url']; ?>" alt="三菱食品株式会社" title="三菱食品株式会社">
				    <?php endif; ?>
				    <p class="sm"><?php echo get_post_meta( get_the_ID(), 'distribution_title_4', true ); ?></p>
			    </div>

			    <div class="col-sm-4 col-6 flex-column"><?php $image = get_field('distribution_logo_5'); ?>
			        <?php if(ICL_LANGUAGE_CODE=='en'): ?>
				        <img src="<?php echo $image['url']; ?>" alt="<?php echo get_post_meta( get_the_ID(), 'distribution_title_5', true ); ?>" title="<?php echo get_post_meta( get_the_ID(), 'distribution_title_5', true ); ?>">
				    <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
				        <img src="<?php echo $image['url']; ?>" alt="日本酒類販売株式会社" title="日本酒類販売株式会社">
				    <?php endif; ?>
				    <p class="sm"><?php echo get_post_meta( get_the_ID(), 'distribution_title_5', true ); ?></p>
			    </div>

			    <div class="col-sm-4 col-6 flex-column"><?php $image = get_field('distribution_logo_6'); ?>
			        <?php if(ICL_LANGUAGE_CODE=='en'): ?>
				        <img src="<?php echo $image['url']; ?>" alt="<?php echo get_post_meta( get_the_ID(), 'distribution_title_6', true ); ?>" title="<?php echo get_post_meta( get_the_ID(), 'distribution_title_6', true ); ?>">
				    <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
				        <img src="<?php echo $image['url']; ?>" alt="株式会社日本アクセス" title="株式会社日本アクセス">
				    <?php endif; ?>
				    <p class="sm"><?php echo get_post_meta( get_the_ID(), 'distribution_title_6', true ); ?></p>
			    </div>
				<div class="vs-20"></div>
		    </div>
		    <div class="vs-20"></div>
		    <?php if(ICL_LANGUAGE_CODE=='ja'): ?><p><?php echo get_post_meta( get_the_ID(), 'distribution_chains_description', true ); ?></p><?php endif; ?>

		    <!-- Section 3 end text -->
		    <p><?php echo get_post_meta( get_the_ID(), 'distribution_chains_description_below', true ); ?></p>

		    <!-- Section 4 -->
	    	<div class="vs-20"></div>
		    <h4 class="black-text"><?php echo get_post_meta( get_the_ID(), 'hakutsuru_title', true ); ?></h4>
		    <p><?php echo get_post_meta( get_the_ID(), 'hakutsuru_description', true ); ?>
		    <div class="vs-10"></div>
		    <?php $image = get_field('hakutsuru_logo'); ?><img class="hakutsuru" src="<?php echo $image['url']; ?>" alt="hakutsuru-logo" title="Hakutsuru Logo">

		</article>

		<div class="vs-60 border"></div>
		<div class="vs-60"></div>

	    <!-- Company Outline -->
		<article>
		  <div id="company-outline" class="anchor"></div>

		    <!-- Table 1 -->
		    <h3><?php if(ICL_LANGUAGE_CODE=='en'): ?>Company outline<?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>会社概要<?php endif; ?></h3>
		    <div class="vs-20"></div>
		    <?php $table = get_field( 'company_outline_table' );if ( $table ) {echo '<table border="0">';if ( $table['header'] ) {echo '<thead>';echo '<tr>';foreach ( $table['header'] as $th ) {echo '<th>';echo $th['c'];echo '</th>';}echo '</tr>';echo '</thead>';}echo '<tbody>';foreach ( $table['body'] as $tr ) {echo '<tr>';foreach ( $tr as $td ) {echo '<td>';echo $td['c'];echo '</td>';}echo '</tr>';}echo '</tbody>';echo '</table>';}
		    ?>

            <!-- Table 2 (Japanese only) -->
		    <?php if(ICL_LANGUAGE_CODE=='ja'): ?>
		    　　　　<div class="vs-40"></div>
		           <h3>酒類販売管理者</h3>
		           <div class="vs-10"></div>
		           <h4 class="black-text">酒類販売管理者標識</h4>
		           <div class="vs-10"></div>
				   <?php $table = get_field( '酒類販売管理者' );if ( $table ) {echo '<table border="0">';if ( $table['header'] ) {echo '<thead>';echo '<tr>';foreach ( $table['header'] as $th ) {echo '<th>';echo $th['c'];echo '</th>';}echo '</tr>';echo '</thead>';}echo '<tbody>';foreach ( $table['body'] as $tr ) {echo '<tr>';foreach ( $tr as $td ) {echo '<td>';echo $td['c'];echo '</td>';}echo '</tr>';}echo '</tbody>';echo '</table>';}
				    ?>
				    <div class="vs-30"></div>
				    <p>アイコン・ユーロパブで働いてみませんか？詳細は、<a class="brown-text" href="<?php echo get_permalink( get_page_by_path( '採用情報' ) ) ?>">採用情報</a>ページをご覧ください。</p>
		    <?php endif; ?>
		</article>

	</div>
</div>

<div class="vs-80"></div>

<!-- FOOTER -->
<?php get_footer(); ?>


<style>
    @media(min-width:768px) {.ikon-banner .banner-img {background-image: url(<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-bar-beer-glassware-desktop.jpg);background-position:50% 10%;} }
    @media(max-width:767px) {.ikon-banner .banner-img {background-image: url(<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-bar-beer-glassware-mobile.jpg);background-position:50% 10%;} }
</style>