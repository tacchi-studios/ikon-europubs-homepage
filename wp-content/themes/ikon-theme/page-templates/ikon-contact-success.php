<?php
/**
 * Template Name: Ikon Contact Success page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header(); ?>

<div class="vs-80 mobile"></div>

<div class="ikon-banner contact-success">
    <div class="ikon-container text-box center-aligned">
        <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-success-icon.svg" width="100" height="100">
        <div class="vs-30"></div>
        <?php if(ICL_LANGUAGE_CODE=='en'): ?>
			<h3>Thank you for contacting Ikon Europubs.</h3>
			<div class="vs-10"></div>
			<p>We will try to get back in touch with you as soon as possible.</p>
			<div class="vs-10"></div>
	    <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
			<h2>お問い合わせありがとうございます。</h2>
			<div class="vs-10"></div>
			<p>採用情報については、お問い合わせフォームよりご連絡ください。 </p>
			<div class="vs-10"></div>
		<?php endif; ?>
	    <div class="vs-10"></div>
	    <a class="button-link" href="<?php echo esc_url( home_url( '/' ) ); ?>">
	        <button class="btn ikon sm wide secondary">
			    <p class="white-text bt">
				    <?php if(ICL_LANGUAGE_CODE=='en'): ?>
				    	Return to home page
				    <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
				    	トップへ戻る
				    <?php endif; ?>
			    </p>
		    </button>
	    </a>
    </div>
</div>

<!-- FOOTER -->
<?php get_footer(); ?>

