<?php
/**
 * Template Name: Ikon Homepage
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header(); ?>

<!-- HOME BANNER -->
<div class="ikon-banner vh720">
    <div class="ikon-container lg text-box home">
         <div class="text-box-empty"></div>
        <div class="text-box-container">
		        <h1 class="white-text"><?php echo get_post_meta( get_the_ID(), 'top_banner_title', true ); ?></h1>
		        <div class="vs-10"></div>
		        <p class="lg white-text"><?php echo get_post_meta( get_the_ID(), 'top_banner_description', true ); ?></p>
        </div>
    </div>
    <div class="banner-img" title="Beer glass on a table at a bar" alt="beer-glass-table-bar"></div>
</div>

<!-- RELIABLE, HIGH-QUALITY BEERS FOR YOUR CUSTOMERS -->
<div class="vs-80"></div>
<div class="ikon-container article center-aligned">
	<h2 style="max-width:750px;"><?php echo get_post_meta( get_the_ID(), 'first_paragraph_title', true ); ?></h2>
	<div class="vs-30"></div>
	<p class="lg"><?php echo get_post_meta( get_the_ID(), 'first_paragraph_description', true ); ?></p>
	<div class="vs-20"></div>
  <div class="three-imports">
    <?php if(ICL_LANGUAGE_CODE=='en'): ?>
		  <div class="col-4"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-beer-bottles-icon.svg" alt="ikon-beer-bottle-icon" title="Ikon beer bottle icon"><p class="black-text"><b>Bottles</b></p></div>
		  <div class="col-4"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-beer-kegs-icon.svg" alt="ikon-beer-kegs-icon" title="Ikon beer kegs icon"><p class="black-text"><b>Kegs</b></p></div>
		  <div class="col-4"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-beer-cans-icon.svg" alt="ikon-beer-cans-icon" title="Ikon beer cans icon"><p class="black-text"><b>Cans</b></p></div>
	  <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
	    <div class="col-4"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-beer-bottles-icon.svg" alt="アイコン・ビールボトル" title="アイコン・ビールボトル"><p class="black-text"><b>瓶ビール</b></p></div>
		  <div class="col-4"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-beer-kegs-icon.svg" alt="アイコン・ビール樽" title="アイコン・ビール樽"><p class="black-text"><b>樽詰めビール</b></p></div>
		  <div class="col-4"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-beer-cans-icon.svg" alt="アイコン・ビール缶" title="アイコン・ビール缶"><p class="black-text"><b>缶ビール</b></p></div>
	  <?php endif; ?>
  </div>
</div>

<div class="vs-80"></div>

<!-- DIFFERENT BEERS FOR DIFFERENT TASTES AND OCCASIONS -->
<!-- Venue images -->
<div class="ikon-container gallery center-aligned">
  <div class="col-lg-4 col-md-4 col-sm-6 col-6 no-pad"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-beer-mexican-food.jpg" alt="beer-mexican-food" title="Mexican food with beer"></div>
  <div class="col-lg-4 col-md-4 col-sm-6 col-6 no-pad"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-beer-glasses-resturant.jpg" alt="ikon-glasses-resturant" title="Beer glasses at a restaurant"></div>
  <div class="col-lg-4 col-md-4 col-sm-6 col-6 no-pad"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-beer-gourmet-food-resturant.jpg" alt="beer-gourmet-food-restaurant" title="Beer and gourmet food at a restaurant"></div>
  <div class="col-lg-4 col-md-4 col-sm-6 col-6 no-pad"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-bar-counter-beers.jpg" alt="bar-counter-beers" title="Beers on a bar counter"></div>
  <div class="col-lg-4 col-md-4 col-sm-6 col-6 no-pad"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-beer-glasses-bar.jpg" alt="beer-glasses-bar" title="Beer glasses at a bar"></div>
  <div class="col-lg-4 col-md-4 col-sm-6 col-6 no-pad"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-bar.jpg" alt="bar" title="Bar"></div>
</div>

<div class="vs-80"></div>

<!-- Text -->
<div class="ikon-container article center-aligned">
	<h2 style="max-width:800px;"><?php echo get_post_meta( get_the_ID(), 'second_paragraph_title', true ); ?></h2>
	<div class="vs-30"></div>
	<p class="lg"><?php echo get_post_meta( get_the_ID(), 'second_paragraph_description', true ); ?></p>
	<div class="vs-40"></div>
</div>

<!-- 3s3c thumbnails -->
<div class="ikon-container thumbnails">
		<div class="col-lg-6 col-md-12 col-12">
		  <div class="label"><h6 class="brown-text"><span><?php if(ICL_LANGUAGE_CODE=='en'): ?>styles<?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>スタイル<?php endif; ?></span></h6><div class="text-box"></div></div>
		  <div class="vs-40"></div>
			<?php $loop = new WP_Query( array( 'post_type' => '3s3c', 'posts_per_page' => -10 ) ); ?>
			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
			  <?php get_template_part( 'loop-templates/ikon-3s3c-style'); ?>
			<?php endwhile; wp_reset_query(); ?>
			<div class="vs-20"></div>
		</div>
		<div class="col-lg-6 col-md-12 col-12">
		  <div class="label"><h6 class="brown-text"><span><?php if(ICL_LANGUAGE_CODE=='en'): ?>categories<?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>カテゴリー<?php endif; ?></span></h6><div class="text-box"></div></div>
		  <div class="vs-40"></div>
			<?php $loop = new WP_Query( array( 'post_type' => '3s3c', 'posts_per_page' => -10 ) ); ?>
			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
			  <?php get_template_part( 'loop-templates/ikon-3s3c-category'); ?>
			<?php endwhile; wp_reset_query(); ?>
			<div class="vs-20"></div>
		</div>
</div>

<div class="vs-60 border"></div>

<!-- 3S3C SECTION -->
<div class="ikon-container showcase">
	<?php $loop = new WP_Query( array( 'post_type' => '3s3c', 'posts_per_page' => -10 ) ); ?>
	<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
	  <?php get_template_part( 'loop-templates/ikon-3s3c'); ?>
	<?php endwhile; wp_reset_query(); ?>
</div>

<div class="vs-100 border"></div>
<div class="vs-80"></div>

<!-- CATALOG CALL TO ACTION -->
<div class="catalog">
  <!--Text + bottle preview -->
	<div class="ikon-container center-aligned">
  	<h2><?php echo get_post_meta( get_the_ID(), 'call_to_action_title', true ); ?></h2>
  	<div class="vs-30"></div>
  	<p class="lg"><?php echo get_post_meta( get_the_ID(), 'call_to_action_description', true ); ?></p>
	</div>
	<div class="lineup">
		<div class="vs-20"></div>
		<?php if(ICL_LANGUAGE_CODE=='en'): ?>
		    <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-catalog-better-selections.jpg" alt="fuller’s-honey-dew-bottle-fuller’s-london-porter-bottle-fuller’s-golden-pride-bottle-krombacher-dark-bottle-krombacher-weisen-bottle-pauliner-salvator-bottle-paulaner hefe-weisbier-dunkel-bottle-fuller’s-imperial-stout-bottle-fuller’s-vintage-ale-bottle-red-hook-long-hammer-ipa-bottle-red-horse-can-san-miguel-can-murphy’s-irish-stout-can-trooper-bottle-trooper-red’n’black-bottle-nest-beer-white-ale-bottle-cali-bottle-cannabia-bottle-salitos-bottle-casa-noble-crystal-bottle-casa-noble-reposado-fuller’s-wild-river-keg-fuller’s-india-pale-ale-keg-fuller’s-honey-dew-keg-paulaner-salvator-keg-paulaner-hefe-weisbier-dunkel-keg-moretti-keg" title="Fuller’s Honey Dew, London porter, Golden Pride bottles  |  Krombacher Weisen bottle  |  Pauliner Salvator, Hefe Weisbier Dunkel bottles  |  Fuller’s Imperial Stout, Vintage ale bottles  |  Red Hook Long Hammer IPA bottle  |  Red Horse bottle  |  San Miguel can  |  Murphy’s Irish Stout can  |  Trooper Red’N’Black bottles  |  Nest beer white ale bottle  |  Cali bottle  |  Cannabia bottle  |  Salitos bottle  |  Casa Noble Crystal, Reposado bottles  |  Fuller’s Wild river, India Pale ale, Honey dew kegs  |  Paulaner Salvator, Hefe Weisbier Dunkel kegs  | Moratti Keg">
		<?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
		    <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-catalog-better-selections.jpg" alt="フラーズハニーデュー-ロンドンポーター-ゴールデンプライド瓶ビール-クロンバッハ-パウラーナーサルバトール-ヘフェヴァイスビアドゥンケル瓶ビール-フラーズインペリアルスタウト-ヴィンテージエール瓶ビール-レッドフックロングハマーIPA瓶ビール-レッドホース瓶ビール-サンミゲール缶ビール-マーフィーズアイリッシュスタウト缶ビール-トルーパーレッドインブラック瓶ビール-ネストビールホワイトエール瓶ビール-カリ瓶ビール-カンナビア瓶ビール-サリトス瓶ビール-カサノブレクリスタル-レポサド瓶ビール-フラーズワイルドリバー-インディアペールエール-ハニーデュー樽詰めビール-パウラーナーサルバトール-ヘフェヴァイスビアドゥンケル樽詰めビール-モレッティー樽詰めビール" title="フラーズ ハニーデュー・ロンドン ポーター・ゴールデン プライド 瓶ビール  |  クロンバッハ 瓶ビール  |  パウラーナー サルバトール・ヘフェ ヴァイスビア ドゥンケル 瓶ビール  |  フラーズ インペリアル スタウト・ヴィンテージ エール 瓶ビール  |  レッドフック ロング ハマー IPA 瓶ビール  |  レッド ホース 瓶ビール  |  サン ミゲール 缶ビール  |  マーフィーズ アイリッシュ スタウト 缶ビール  |  トルーパー レッド イン ブラック 瓶ビール  |  ネスト ビール ホワイト エール 瓶ビール  |  カリ 瓶ビール  |  カンナビア 瓶ビール  |  サリトス 瓶ビール  |  カサ ノブ レクリスタル・レポサド 瓶ビール  |  フラーズ ワイルド リバー・インディア ペール エール・ハニーデュー 樽詰めビール  |  パウラーナー サルバトール・ヘフェ ヴァイスビア ドゥンケル 樽詰めビール  |  モレッティー 樽詰めビール">
	    <?php endif; ?>
	</div>
	<!--Download section -->
	<div class="ikon-container center-aligned preview">
		<div class="vs-60"></div>
		<?php if(ICL_LANGUAGE_CODE=='en'): ?>
			<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-catalog.jpg"alt="ikon-europubs-imported-beer-catalog" title="Ikon Europubs imported beer catalog">
		<?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
			<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-catalog.jpg"alt="アイコンユーローパブ輸入ビールカタログ" title="アイコンユーローパブ輸入ビールカタログ">
		<?php endif; ?>
		<div class="vs-30"></div>
		<a target="_blank" class="button-link" href="<?php echo get_site_url(); ?>/wp-content/uploads/ikon-europubs-catalog.pdf">
			<button class="btn ikon primary">
			    <p class="bt white-text">
				    <?php if(ICL_LANGUAGE_CODE=='en'): ?>
				    	Download catalog
				    <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
				    	カタログをダウンロードする
				    <?php endif; ?>
			    </p>
			</button>
		</a>
	</div>
</div>

<div class="vs-80"></div>

<!-- CONTACT CALL TO ACTION -->
<div class="ikon-banner vh320">
    <div class="ikon-container text-box center-aligned">
      <?php if(ICL_LANGUAGE_CODE=='en'): ?>
        <h2 class="white-text">Contact Ikon</span></h2>
        <p class="lg white-text">Please contact us for more information about any of our beers. </p>
      <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
        <p class="lg white-text">ビールに関するさらに詳しい情報については、弊社にお気軽にお問い合わせください。</p>
      <?php endif; ?>
  　　　 <a class="button-link" href="<?php echo get_permalink( get_page_by_path( 'contact' ) ) ?>">
			<button class="btn ikon primary">
			    <p class="white-text bt">
				    <?php if(ICL_LANGUAGE_CODE=='en'): ?>
				    	REQUEST BEER INFORMATION
				    <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
				    	ビールの情報をリクエストする
				    <?php endif; ?>
			    </p>
			</button>
		</a>
    </div>
    <div class="banner-img contact" title="Bartender pouring beer from a tab at a bar" alt="bartender-pouring-beer-tab-bar"></div>
</div>

<!-- FOOTER -->
<?php get_footer(); ?>



<style>
    @media(min-width:1370px) {.ikon-banner .banner-img {background-position:70% 0%;} }
    @media(max-width:1369px) and (min-width:1191px) {.ikon-banner .banner-img {background-position:40% 0%;} }
    @media(max-width:1190px) and (min-width:992px) {.ikon-banner .banner-img {background-position:60% 0%;} }

    /* Home desktop image */
    @media(min-width:992px) {
	    .ikon-banner .banner-img {
	    	background-image: url(<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-home-beer-banner-desktop.jpg);
	    }
    }

    /* Home tablet image */
    @media(max-width:991px) and (min-width:544px) {
	    .ikon-banner .banner-img {
	    	background-image: url(<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-home-beer-banner-tablet.jpg);
	    	background-position:50% 20%;
	    }
    }

    /* Home mobile image */
    @media(max-width:543px) {
	    .ikon-banner .banner-img {
	    	background-image: url(<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-home-beer-banner-mobile.jpg);
	    	background-position:50% 0%;
	    }
    }

    /* Contact CTA desktop image */
    @media(min-width:768px) {
	    .ikon-banner .banner-img.contact {
	    	background-image: url(<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-bar-tab-desktop.jpg);
	    	background-position:45% 50%;
	    }
    }

    /* Contact CTA mobile image */
    @media(max-width:767px) {
	    .ikon-banner .banner-img.contact {
	    	background-image: url(<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-bar-tab-mobile.jpg);
	    	background-position:50% 50%;
	    }
    }
</style>