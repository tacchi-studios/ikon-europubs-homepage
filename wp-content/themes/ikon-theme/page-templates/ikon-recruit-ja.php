<?php
/**
 * Template Name: Ikon Recruit (JP only)
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header(); ?>

<div class="vs-80 mobile"></div>
<div class="vs-60 mobile"></div>
<div class="vs-40 tablet"></div>
<div class="vs-60"></div>


<div class="ikon-container md">
    <!-- Job posts -->
    <h2>アイコン・ユーロパブの採用情報</h2>
    <div class="vs-30"></div>
	<?php $loop = new WP_Query( array( 'post_type' => 'jobs', 'posts_per_page' => -10 ) ); ?>
	<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
	  <?php get_template_part( 'loop-templates/ikon-jobs'); ?>
	<?php endwhile; wp_reset_query(); ?>
</div>


<div class="ikon-container md">
	<!-- CALL TO ACTION -->
	<p class="lg">採用情報については、<a class="brown-text" href="<?php echo get_permalink( get_page_by_path( 'contact' ) ) ?>">お問い合わせフォーム</a> よりご連絡ください。 </p>
</div>

<div class="vs-80"></div>


<!-- FOOTER -->
<?php get_footer(); ?>


<style>
    @media(min-width:544px) {
	    .ikon-banner .banner-img.contact {
	    	background-image: url(<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-bar-tab.jpg);
	    	background-position:100% 15%;
	    }
    }
    @media(max-width:543px) {
	    .ikon-banner .banner-img.contact {
	    	background-image: url(<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-bar-tab.jpg);
	    	background-position:70% 50%;
	    }
    }
    button {
        margin: 0;
    }
</style>