<?php
/**
 * Template Name: Ikon Company (backup) page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header(); ?>

<!-- COMPANY BANNER-->
<div class="vs-80 mobile"></div>
<div class="ikon-banner vh280">
    <div class="banner-img"></div>
</div>

<div class="vs-60"></div>

<!-- CONTAINER -->
<div class="ikon-container">
    <!-- Menu -->
	<div class="menu-container no-pad">
    <div class="company-menu">
      <a class="about toggled transition" href="#about-ikon-europubs"><div class="square-arrow"></div><p class=""><i class="fa fa-angle-right dark-yellow-text" aria-hidden="true"></i>&nbsp;&nbsp;<?php if(ICL_LANGUAGE_CODE=='en'): ?><b>About Ikon Europubs</b><?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>会社情報<?php endif; ?></b></p></a>
      <a class="company transition" href="#company-outline"><div class="square-arrow"></div><p class=""><i class="fa fa-angle-right dark-yellow-text" aria-hidden="true"></i>&nbsp;&nbsp;<?php if(ICL_LANGUAGE_CODE=='en'): ?><b>Company Outline</b><?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>会社概要<?php endif; ?></p></a>
    </div>

	</div>
	<!-- Content -->
	<div class="article-container no-pad">

	  <!-- About Ikon Europubs -->
		<article>
		  <div id="about-ikon-europubs" class="anchor"></div>

	    <!-- Section 1 -->
	    <?php if(ICL_LANGUAGE_CODE=='en'): ?>
		    <h3>Ikon brings the great beers of the world to Japan</h3>
		    <div class="vs-20"></div>
		    <p>At Ikon we are proud to offer one of the strongest and most diverse portfolios of imported beers and ciders in Japan.  We import a wide range of draft beers from countries around the world, and we make every effort to ensure that all our beers - whether they come by the keg, bottle or can - will be enjoyed by your customers in the best possible condition.</p>
		    <p>Ikon specializes in premium beers - well-loved international brands that are often the number one beer in their market or category.  As the official representatives of some of the world's top brewing companies, we work with these breweries to actively market and promote their beers in Japan.  In support of these efforts we also supply matching glassware for many of the beers we carry.</p>
		    <p>First launched in 1970, Ikon was an early pioneer in the beer-import business, and over the years we have built up the most comprehensive domestic and global distribution network in the industry, covering thousands of liquor suppliers and working with all major national distributors to reach every corner of Japan. </p>
		    <div class="vs-20"></div>
	    <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
		    <h3>Ikon Europubsは、世界中のトップクラスのビールを日本にお届けします</h3>
		    <div class="vs-10"></div>
		    <p>Ikon Europubsの輸入ビール、そしてシードルの商品ラインアップは日本で最もヴァラエティに富んだ内容です。商品は、瓶、缶、そして、樽でもお届けします。ビールの旨味を最大限に生かせるように、商品のコンディションには細心の注意を払っています。</p>
		    <p>Ikon Europubsは、プレミアム・ビールに特化しています。私たちの販売する商品は、市場、業界で人気ナンバーワンになったビールが多く、国際的な認知度も高いものばかりです。世界でトップクラスのビール会社の正規代理店として、私たちは積極的に日本でプロモーションを行っており、各ブランドのオフィシャル・グラスもご用意しております。</p>
		    <p>1970年に創業したIkon Europubsは、日本のおけるビール輸入業者のパイオニア的な存在です。私たちは長年に渡り、国内外で最良の流通ネットワークを築いています。また、多数の卸売業者と取引を行っているとともに、日本国中に商品を流通させるために、国内の主要な販売店と協働しています。 </p>
		    <div class="vs-20"></div>
	    <?php endif; ?>

	    <!-- Section 2 -->
	    <?php if(ICL_LANGUAGE_CODE=='en'): ?>
		    <h4 class="black-text"><b>Full service to support your business</b></h4>
		    <p>Our driving commitment is to deliver all our products in optimal condition, just as they would be in their home markets.  But we also care what happens to our products even after delivery, and we believe in providing the services necessary to ensure their best possible condition and quality on an ongoing basis. </p>
		    <p>We help procure, install and maintain serving equipment, and we provide advice on design and construction of bars and serving facilities.  We also help organize and supply local beer events.  We want as many people as possible to experience and enjoy the great collection of beers that we bring to Japan.</p>
		    <div class="vs-20"></div>
	    <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
		    <h4 class="black-text">常に最良のサポートをご提供いたします。</h4>
		    <p>私たちのコミットメントは、輸入ビールを現地で味わうレベルと同様に、最良のコンディションでお届けすることです。商品をお届けした後もケアを怠りません。</p>
		    <p>私たちは、ビールをサーブする機器を設置するとともに、メンテナンスも行います。また、バーの設計、機器についてのコンサルテーションも請け負っております。<br>ビール関連のイベントも積極的に行っており、一人でも多くのお客様が私たちの商品を楽しんでいただけることを願っています。</p>
		    <div class="vs-20"></div>
	    <?php endif; ?>

	    <!-- Section 3 -->
	    <?php if(ICL_LANGUAGE_CODE=='en'): ?>
		    <h4 class="black-text"><b>Ikon's nationwide distribution chain</b></h4>
		    <p>Ikon works with ever major nationwide distributor in Japan.  Through these distributors, Ikon's beers reach tens of thousands of local suppliers and liquor shops throughout the country.</p>
		    <div class="vs-40"></div>
	    <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
		    <h4 class="black-text">Ikon Europubsが取引を行っている主な販売店は以下の通りです。 </h4>
		    <div class="vs-10"></div>
	    <?php endif; ?>

	    <!-- Company logos -->
	    <?php if(ICL_LANGUAGE_CODE=='en'): ?>
		    <div class="logo-container center-aligned">
		      <div class="vs-30"></div>
			    <div class="col-sm-4 col-6"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-kokubu-logo.jpg"><p class="sm">Kokubu Group</p></div>
			    <div class="col-sm-4 col-6"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-mitsui-foods-logo.jpg"><p class="sm">Mitsui Foods</p></div>
			    <div class="col-sm-4 col-6"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-itochu-shokuhin-logo.jpg"><p class="sm">Itochu Shokuhin</p></div>
			    <div class="col-sm-4 col-6"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-mitsubishi-shokuhin-logo.jpg"><p class="sm">Mitsubishi Shokuhin</p></div>
			    <div class="col-sm-4 col-6"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-nihon-shurui-hanbai-logo.jpg"><p class="sm">Nihon Shurui Hanbai</p></div>
			    <div class="col-sm-4 col-6"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-nippon-access-logo.jpg"><p class="sm">Nippon Access</p></div>
			    <div class="vs-30"></div>
		    </div>
	    <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
		    <div class="logo-container center-aligned">
		      <div class="vs-30"></div>
			    <div class="col-sm-4 col-6"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-kokubu-logo.jpg"><p class="sm">国分グループ本社株式会社</p></div>
			    <div class="col-sm-4 col-6"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-mitsui-foods-logo.jpg"><p class="sm">三井食品株式会社</p></div>
			    <div class="col-sm-4 col-6"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-itochu-shokuhin-logo.jpg"><p class="sm">伊藤忠食品株式会社</p></div>
			    <div class="col-sm-4 col-6"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-mitsubishi-shokuhin-logo.jpg"><p class="sm">三菱食品株式会社</p></div>
			    <div class="col-sm-4 col-6"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-nihon-shurui-hanbai-logo.jpg"><p class="sm">日本酒類販売株式会社</p></div>
			    <div class="col-sm-4 col-6"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-nippon-access-logo.jpg"><p class="sm">株式会社 日本アクセス</p></div>
			    <div class="vs-30"></div>
		    </div>
		  <?php endif; ?>

      <?php if(ICL_LANGUAGE_CODE=='ja'): ?>
      <div class="vs-20"></div>
		  <p>上記の販売店を通じて、私たちの商品は、日本国内の数多くの卸店と小売店に商品を流通しています。</p>
		  <?php endif; ?>

	    <!-- Section 4 -->
	    <?php if(ICL_LANGUAGE_CODE=='en'): ?>
	    	<div class="vs-40"></div>
		    <h4 class="black-text"><b>Ikon and Hakutsuru</b></h4>
		    <p>In April 2017 Ikon Europubs became a subsidiary of Hakutsuru Sake Brewing Co. Ltd.  Founded in 1743, the Kobe-based Hakutsuru is today the largest sake producer in Japan.</p>
		    <p>As a new member of the Hakutsuru family Ikon will continue in its mission of bringing the highest quality products from around the globe to the Japanese consumer, working with Hakutsuru to reach new customers and markets throughout Japan.</p>
		    <img class="hakutsuru" src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-hakutsuru-sake-logo.jpg">
	    <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
	    	<div class="vs-20"></div>
		    <h4 class="black-text">Ikon Europubsは、白鶴酒造株式会社の子会社です。</h4>
		    <p>2017年4月、Ikon Europubs は白鶴酒造株式会社の子会社となりました。1743年に創業された白鶴酒造株式会社は、神戸を拠点とする日本で最大規模を誇る清酒メーカーです。</p>
		    <p>白鶴酒造の子会社として、私たちはこれからも世界各国の最高品質のビールを日本のお客様にお届けするというミッションを担っていきます。白鶴酒造とパートナーシップを組むことで、国内の新たな顧客と市場が開拓できることと私たちは信じています。</p>
		    <img class="hakutsuru" src="<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-hakutsuru-sake-logo.jpg">
		  <?php endif; ?>

		</article>

		<div class="vs-60 border"></div>
		<div class="vs-60"></div>

	    <!-- Company Outline -->
		<article>
		  <div id="company-outline" class="anchor"></div>

	    <!-- Table 1 -->
	    <?php if(ICL_LANGUAGE_CODE=='en'): ?>
		    <h3>Company Outline</h3>
		    <div class="vs-20"></div>
		    <table>
			    <tr><td><p>Company name</p></td><td><p>Ikon Europubs KK</p></td></tr>
			    <tr><td><p>President</p></td><td><p>Trevor Allen</p></td></tr>
			    <tr><td><p>Director</p></td><td><p>Kenji Kano, Kyoichi Kondo</p></td></tr>
			    <tr><td><p>Auditor</p></td><td><p>Hideki Masuda</p></td></tr>
			    <tr><td><p>Advisor</p></td><td><p>Shigenobu Fujio</p></td></tr>
			    <tr><td><p>Established</p></td><td><p>Ikon KK - Feb 1970<br>Ikon Europubs KK - Apr 2001<br>Business Operations Integrated - Sep 2008<br>Became subsidiary of Hakutsuru Sake Brewing Co. Ltd.</p></td></tr>
			    <tr><td><p>Web Address</p></td><td><p>http://www.ikon-europubs.com/</p></td></tr>
			    <tr><td><p>Headquarters</p></td><td><p>14-5 Daikyocho, Shinjuku-ku, Tokyo 160-0015</p></td></tr>
			    <tr><td><p>Contacts</p></td><td><p>Tel: 03-5369-3601<br>Fax:03-5369-3602</p></td></tr>
			    <tr><td><p>Main Banks</p></td><td><p>Mizuho Bank Ltd.; Sumitomo Mitsui Banking Corp.; Bank of Tokyo-Mitsubishi (UFJ)</p></td></tr>
			    <tr><td><p>Main Products </p></td><td><p><b>Wholesale</b> - Beer, cider, wine and foodstuffs</p><p><b>Consulting</b> - Specialist advice and support for beverage menus; support for design and construction of facilities; international brand development</p></td></tr>
			  </table>
		  <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
		    <h3>会社概要</h3>
		    <div class="vs-10"></div>
		    <table>
			    <tr><td><p class="sm">社名</p></td><td><p class="sm">アイコンユーロパブ株式会社</p></td></tr>
			    <tr><td><p class="sm">代表取締役社長</p></td><td><p class="sm">トレーバー・アレン</p></td></tr>
			    <tr><td><p class="sm">取締役</p></td><td><p class="sm">嘉納健二、近藤恭一</p></td></tr>
			    <tr><td><p class="sm">監査役</p></td><td><p class="sm">増田秀樹、藤尾成伸</p></td></tr>
			    <tr><td><p class="sm">創業</p></td><td><p class="sm">1970年2月 アイコン株式会社<br>2001年4月 ユーロパブインポーツ株式会社<br>2008年9月 合併により社名変更<br>2017年4月 白鶴酒造株式会社の子会社となる。</p></td></tr>
			    <tr><td><p class="sm">Webアドレス</p></td><td><p class="sm">http://www.ikon-europubs.com/</p></td></tr>
			    <tr><td><p class="sm">所在地</p></td><td><p class="sm">〒160-0015 東京都新宿区大京町14-5</p></td></tr>
			    <tr><td><p class="sm">連絡先</p></td><td><p class="sm">Tel: 03-5369-3601<br>Fax:03-5369-3602</p></td></tr>
			    <tr><td><p class="sm">取引銀行</p></td><td><p class="sm">みずほ銀行、三井住友銀行、三菱東京UFJ銀行</p></td></tr>
			    <tr><td><p class="sm">事業内容</p></td><td><p class="sm">ビール、サイダー、ワイン等酒類・食品類<br>コンサルティング業務<br>アイリッシュパブやブリティッシュパブの店舗開店、 デザイン、工事等のコンサルティング業務、海外ブランド品の市場開発、海外ビールの市場開発</p></td></tr>
			  </table>
			  <div class="vs-60"></div>

			  <!-- Table 2 -->
		    <h3>酒類販売管理者</h3>
		    <div class="vs-10"></div>
		    <table>
			    <tr><td><p class="sm">販売場の名称及び所在地</p></td><td><p class="sm">アイコン・ユーロパブ(株) 	 東京都新宿区大京町14-5 </p></td></tr>
			    <tr><td><p class="sm">酒類販売管理者の氏名</p class="sm"></td><td><p　class="sm">高山和也 </p></td></tr>
			    <tr><td><p class="sm">酒類販売管理研修受講年月日</p class="sm"></td><td><p　class="sm">2017/06/09</p></td></tr>
			    <tr><td><p class="sm">次回研修の受講期限</p class="sm"></td><td><p class="sm">2020/06/08</p></td></tr>
			    <tr><td><p class="sm">研修実施団体名</p class="sm"></td><td><p　class="sm">一般社団法人新日本スーパーマーケット協会</p class="sm"></td></tr>
			  </table>

        <div class="vs-20"></div>
			  <p>アイコン・ユーロパブで働いてみませんか？詳細は、<a class="brown-text" href="<?php echo get_permalink( get_page_by_path( '採用情報' ) ) ?>">採用情報</a>ページをご覧ください。</p>
			<?php endif; ?>

		</article>

	</div>
</div>

<div class="vs-80"></div>

<!-- FOOTER -->
<?php get_footer(); ?>


<style>
    @media(min-width:768px) {
	    .ikon-banner .banner-img {
	    	background-image: url(<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-bar-beer-glassware.jpg);
	    	background-position:50% 10%;
	    }
    }
    @media(max-width:767px) {
	    .ikon-banner .banner-img {
	    	background-image: url(<?php bloginfo('stylesheet_directory'); ?>/assets/ikon-bar-beer-glassware-mobile.jpg);
	    	background-position:50% 10%;
	    }
    }
</style>