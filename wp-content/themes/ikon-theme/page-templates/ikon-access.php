<?php
/**
 * Template Name: Ikon Access Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header(); ?>


<div class="vs-80 mobile"></div>
<div class="vs-40 tablet"></div>
<div class="vs-60"></div>
<div class="ikon-container showcase">
    <div class="wrapper col-md-12 no-pad">
	    <div class="col-md-7 col-12 no-pad map">
	        <a target="_blank" href="https://www.google.com/maps?hl=en&ie=UTF8&q=35.684944,139.717101&z=16">
	            <div class="map-responsive">
	                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2676EO3VjLTv5WP905w5oO3MMdO9qawI&callback=initMap"></script><div class="map-style" style='overflow:hidden;height:400px;width:100&;'><div id='gmap_canvas' style='height:400px;width:100%;'></div><div><small></small></div><div><small></small></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div>
					<script type='text/javascript'>function init_map(){var myOptions = {zoom:16,mapTypeControl:false,center:new google.maps.LatLng(35.684944,139.717101),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(35.684944,139.717101)});infowindow = new google.maps.InfoWindow({content:'<strong>Ikon Europubs</strong><br>35.684944,139.717101<br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
	            </div>
	        </a>
		</div>
	    <div class="col-md-5 col-12 no-pad">
		<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'loop-templates/ikon-access' ); ?>
	    <?php endwhile; // end of the loop. ?>
			<div class="vs-20"></div>
			  <?php if(ICL_LANGUAGE_CODE=='en'): ?>
				  <a target="_blank" href="https://www.google.co.jp/maps/place/35%C2%B041'05.8%22N+139%C2%B043'01.6%22E/@35.6849303,139.7171331,21z/data=!4m5!3m4!1s0x0:0x0!8m2!3d35.684944!4d139.717101=en">
					  <button class="btn ikon secondary sm"><p class="center-aligned sm">Google Maps</p></button>
				  </a>
			  <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
				  <a target="_blank" href="https://www.google.co.jp/maps/place/35%C2%B041'05.8%22N+139%C2%B043'01.6%22E/@35.6849303,139.7171331,21z/data=!4m5!3m4!1s0x0:0x0!8m2!3d35.684944!4d139.717101=ja">
				   <button class="btn ikon secondary sm"><p class="center-aligned sm">Googleマップ</p></button>
				  </a>
			  <?php endif; ?>
		</div>
	</div>
</div>

<div class="vs-80"></div>

<!-- FOOTER -->
<?php get_footer(); ?>

