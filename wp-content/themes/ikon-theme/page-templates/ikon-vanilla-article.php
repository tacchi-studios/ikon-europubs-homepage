<?php
/**
 * Template Name: Ikon Vanilla Article
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header(); ?>

<div class="vs-80 mobile"></div>
<div class="vs-60 mobile"></div>
<div class="vs-40 tablet"></div>
<div class="vs-60"></div>

<div class="ikon-container md">
	<?php while ( have_posts() ) : the_post(); ?>
	<?php get_template_part( 'loop-templates/content', 'single' ); ?>
    <?php endwhile; // end of the loop. ?>
</div>

<div class="vs-80"></div>

<!-- FOOTER -->
<?php get_footer(); ?>
