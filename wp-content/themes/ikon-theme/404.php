<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package understrap
 */

get_header(); ?>

<div class="vs-80 mobile"></div>

<div class="ikon-banner contact-success">
    <div class="ikon-container text-box center-aligned md">
        <div class="vs-30"></div>
	      <?php if(ICL_LANGUAGE_CODE=='en'): ?>
	        <h3 class="black-text">Oops! Looks like we couldn't find the page you were looking for...</span></h3>
	        <div class="vs-20"></div>
	        <p class="lg black-text">Use the navigation or contact us to find out more about Ikon Europubs.</p>
	      <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
	        <h3 class="black-text">大変申し分けございませんが、探しているページが見つかりませんでした。</span></h3>
	        <div class="vs-20"></div>
	        <p class="lg black-text">サイトのメニューをご利用なさるか、アイコン・ユーロパブへお問い合わせください。</p>
	      <?php endif; ?>
	    <div class="vs-30"></div>
	    <a class="button-link" href="<?php echo esc_url( home_url( '/' ) ); ?>">
	        <button class="btn ikon sm wide secondary">
			    <p class="white-text bt">
				    <?php if(ICL_LANGUAGE_CODE=='en'): ?>
				    	Homepage
				    <?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>
				    	ホームページ
				    <?php endif; ?>
			    </p>
		    </button>
	    </a>
    </div>
</div>

<!-- FOOTER -->
<?php get_footer(); ?>

