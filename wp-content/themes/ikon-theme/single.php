<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

get_header(); ?>

<!-- 3S3C SECTION -->
<div class="ikon-container showcase">
	<?php get_template_part( 'loop-templates/content', 'single' ); ?>
</div>

<div class="vs-100"></div>

<!-- 3s3c thumbnails -->
<div class="ikon-container thumbnails">
		<div class="col-lg-6 col-md-12 col-12">
		  <div class="label"><h6 class="brown-text"><span><?php if(ICL_LANGUAGE_CODE=='en'): ?>styles<?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>スタイル<?php endif; ?></span></h6><div class="text-box"></div></div>
		  <div class="vs-40"></div>
			<?php $loop = new WP_Query( array( 'post_type' => '3s3c', 'posts_per_page' => -10 ) ); ?>
			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
			  <?php get_template_part( 'loop-templates/ikon-3s3c-style-link'); ?>
			<?php endwhile; wp_reset_query(); ?>
			<div class="vs-20"></div>
		</div>
		<div class="col-lg-6 col-md-12 col-12">
		  <div class="label"><h6 class="brown-text"><span><?php if(ICL_LANGUAGE_CODE=='en'): ?>categories<?php elseif(ICL_LANGUAGE_CODE=='ja'): ?>カテゴリー<?php endif; ?></span></h6><div class="text-box"></div></div>
		  <div class="vs-40"></div>
			<?php $loop = new WP_Query( array( 'post_type' => '3s3c', 'posts_per_page' => -10 ) ); ?>
			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
			  <?php get_template_part( 'loop-templates/ikon-3s3c-category-link'); ?>
			<?php endwhile; wp_reset_query(); ?>
			<div class="vs-20"></div>
		</div>
</div>

<div class="vs-60"></div>
<div class="vs-20"></div>

<?php get_footer(); ?>
